; type direction = map | unmap

; type warp t = direction -> t -> t

; direction -> bool
(define warp-fwd?
  (lambda (s) (eq? s 'map)))

; float -> float -> warp float
(define warp-linear
  (lambda (minima maxima)
    (let ((range (- maxima minima)))
      (lambda (direction value)
	(if (warp-fwd? direction)
	    (+ (* value range) minima)
	    (/ (- value minima) range))))))

; int -> int -> warp int
(define warp-linear-integer
  (lambda (minima maxima)
    (let ((w (warp-linear minima maxima)))
      (lambda (direction value)
	(round (w direction value))))))

; float -> float -> warp float
(define warp-exponential
  (lambda (minima maxima)
    (let ((ratio (/ maxima minima)))
      (lambda (direction value)
	(if (warp-fwd? direction)
	    (* (expt ratio value) minima)
	    (/ (log (/ value minima)) (log ratio)))))))

; float -> (float -> float -> warp float)
(define warp-make-warp-curve
  (lambda (curve)
    (lambda (minima maxima)
      (let ((range (- maxima minima)))
	(if (< (abs curve) 0.001)
	    (warp-linear minima range)
	    (let* ((grow (exp curve))
		   (a (/ range (- 1.0 grow)))
		   (b (+ minima a)))
	      (lambda (direction value)
		(if (warp-fwd? direction)
		    (- b (* a (expt grow value)))
		    (/ (log (/ (- b value) a)) curve)))))))))

; float -> float -> warp float
(define warp-cosine
  (lambda (minima maxima)
    (let* ((range (- maxima minima))
	   (linear (warp-linear minima range)))
      (lambda (direction value)
	(if (warp-fwd? direction)
	    (linear 'map (- 0.5 (* (cos (* pi value)) 0.5)))
	    (/ (acos (- 1.0 (* (linear 'unmap value) 2))) pi))))))

; float -> float -> warp float
(define warp-sine
  (lambda (minima maxima)
    (let* ((range (- maxima minima))
	   (linear (warp-linear minima range)))
      (lambda (direction value)
	(if (warp-fwd? direction)
	    (linear 'map (sin (* (/ pi 2) value)))
	    (/ (asin (linear 'unmap value)) (/ pi 2)))))))

; _ -> _ -> warp float
(define warp-fader
  (lambda (minima maxima)
    (lambda (direction value)
      (if (warp-fwd? direction)
	  (* value value)
	  (sqrt value)))))

; _ -> _ -> warp float
(define warp-db-fader
  (lambda (minima maxima)
    (lambda (direction value)
      (if (warp-fwd? direction)
	  (if (zero? value)
	      -180
	      (amp-db (* value value)))
	  (sqrt (db-amp value))))))

; symbol -> (float -> float -> warp float)
(define symbol->warp
  (lambda (s)
    (case s
      ((lin linear) warp-linear)
      ((exp exponential) warp-exponential)
      ((sin) warp-sine)
      ((cos) warp-cosine)
      ((amp) warp-fader)
      ((db) warp-db-fader)
      (else (error "symbol->warp" "unknown symbol" s)))))

; float -> (float -> float -> warp float)
(define number->warp
  (lambda (n) (warp-make-warp-curve n)))
