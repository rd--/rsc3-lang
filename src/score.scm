; An Sc3 score is a list of osc bundles. The timestamps are given in
; seconds where zero is the start of the score. An Osc file is a
; binary file format understood by the Sc3 synthesis server, and
; consists of a sequence of length prefixed Osc bundles.

; [osc] -> bytevector
(define encode-score
  (lambda (l)
    (flatten-bytevectors
     (map
      (lambda (b)
	(let ((v (flatten-bytevectors (encode-bundle-ntp b))))
	  (list (encode-i32 (bytevector-length v)) v)))
      l))))
