; (time -> delta-time) -> time -> ()
(define deltaTimeRescheduler
  (lambda (f t)
    (begin
      (pauseThreadUntil t)
      (let ((r (f t)))
        (when (number? r) (deltaTimeRescheduler f (+ t r)))))))

; ugen:trig -> ugen -> ugen -> done-action -> ugen
;
; (Mul (SinOsc 440 0) (Mul (overlapEnv 1 5 1 1) 0.1))
(define overlapEnv
  (lambda (gt s t a)
    (let* ((c 2) ; sin
           (p (EnvLinen t s t 1 (list c c c))))
      (EnvGen gt 1 0 1 a p))))

; ugen -> ugen:trig -> ugen -> ugen -> done-action -> ugen
;
; (withEnv (Mul (SinOsc 440 0) 0.1) 1 1 0.5 1)
(define withEnv
  (lambda (gr gt s t a)
    (Out 0 (Mul gr (overlapEnv gt s t a)))))

; overlap-texture-t -> float
(define overlapTextureIot
  (lambda (s t o)
    (/ (+ (* t 2) s) o)))

; t|(() -> t) -> t
(define from-maybe-closure
  (lambda (x)
    (if (procedure? x) (x) x)))

; (sustainTime:real?, transitionTime:real?, interOffsetTime: real|(() -> real), overlaps:int, ugen|(() -> ugen)) -> thread
(define generalised-texture
  (lambda (s t iot n u)
    (lambda (fd)
      (let ((f (lambda (_)
                 (if (> n 0)
                     (begin
                       (set! n (- n 1))
                       (let* ((u* (from-maybe-closure u))
                              (u** (if (not s) (Out 0 u*) (withEnv u* 1 s t removeSynth))))
                         (playAt fd u** -1 addToHead 1))
                       (from-maybe-closure iot))
                     #f))))
        (deltaTimeRescheduler f (currentTime))))))

; [either real (() -> real),int] -> either ugen (() -> ugen) -> (fd -> ())
(define spawnUgen
  (lambda (k u)
    (let* ((iot (list-ref k 0))
           (n (list-ref k 1)))
      (generalised-texture #f #f iot n u))))

; overlap-texture-t -> (() -> ugen) -> (fd -> ())
(define overlapTexture
  (lambda (k u)
    (let* ((s (list-ref k 0))
           (t (list-ref k 1))
           (o (list-ref k 2))
           (n (list-ref k 3))
           (iot (overlapTextureIot s t o)))
      (generalised-texture s t iot n u))))

; overlap-texture-t -> ugen -> (fd -> ())
(define overlapTextureUgen
  (lambda (k u)
    (overlapTexture k (lambda () u))))

(define overlap
  (lambda (eventFunc sustainTime transitionTime numberOfOverlaps)
    (withSc3
     (generalised-texture
      sustainTime
      transitionTime
      (overlapTextureIot sustainTime transitionTime numberOfOverlaps)
      dinf
      eventFunc))))

; xfade-texture-t -> float
(define xfadeTextureIot
  (lambda (s t o)
    (/ (+ (* t 2) s) o)))

; xfade-texture-t -> (() -> ugen) -> (fd -> ())
(define xfadeTexture
  (lambda (k u)
    (let* ((s (list-ref k 0))
           (t (list-ref k 1))
           (n (list-ref k 2))
           (iot (+ s t)))
      (generalised-texture s t (lambda () iot) n u))))

; xfade-texture-t -> ugen -> (fd -> ())
(define xfadeTextureUgen
  (lambda (k u)
    (xfadeTexture k (lambda () u))))

(define postProcessUgen
  (lambda (nc f)
    (lambda (fd)
      (let ((u (ReplaceOut 0 (f (In nc 0)))))
        (playAt fd u -1 addToTail 1)))))

; (sustainTime:float|ugen, transitionTime:float|ugen, overlaps:int, graphFunc:(ugen:trig -> ugen)) -> ugen
(define overlapTextureGraph
  (lambda (s t n gr)
    (mixFill
     n
     (lambda (i)
       (let* ((tr (kr: (Impulse (/ 1 (+ s t t)) (/ i n))))
             (snd (gr tr)))
         (Mul snd (overlapEnv tr s t doNothing)))))))

; (graphFunc, sustainTime, transitionTime, numberOfOverlaps)
(define OverlapTexture
  (lambda (gr s t n)
    (overlapTextureGraph s t n gr)))
