(define au-magic #x2e736e64)

; data encoding
(define au-unspecified 0)
(define au-mulaw8 1)
(define au-linear8 2)
(define au-linear16 3)
(define au-linear24 4)
(define au-linear32 5)
(define au-float 6)
(define au-double 7)

; encoding -> int
;
; (== (au-size-of au-float) 4)
(define au-size-of
  (lambda (e)
    (cond ((= e au-unspecified) (error "au-size-of" "unspecified encoding"))
          ((= e au-mulaw8) 1)
          ((= e au-linear8) 1)
          ((= e au-linear16) 2)
          ((= e au-linear24) 3)
          ((= e au-linear32) 4)
          ((= e au-float) 4)
          ((= e au-double) 8)
          (else (error "au-size-of" "illegal encoding")))))

; int -> encoding -> int -> int -> au-header
;
; (au-mk-hdr 512 au-float 48000 1)
(define au-mk-hdr
  (lambda (nf enc sr nc)
    (let ((nb (* nf nc (au-size-of enc))))
      (map encode-u32 (list au-magic 28 nb enc sr nc 0)))))

; type encoder t = (encoding,t -> bytevector)
(define au-f32 (list au-float encode-f32))
(define au-f64 (list au-double encode-f64))

; encoder t -> int -> int -> filepath -> [[t]] -> ()
(define write-snd-file
  (lambda (e sr nc fn d)
    (let* ((enc (car e))
           (encdr (cadr e))
           (nf (/ (length d) nc))
           (fd (open-file-output-port fn (file-options) (buffer-mode block) #f))
           (wr (lambda (x) (put-bytevector fd x))))
      (for-each wr (au-mk-hdr nf enc sr nc))
      (for-each wr (map encdr d))
      (close-port fd))))
