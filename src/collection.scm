; (t -> t -> t) -> (int -> t -> t -> [t])
(define series-with
  (lambda (plus)
    (letrec ((f (lambda (n i j)
                  (if (equal? n 0)
                      (list)
                      (cons i (f (- n 1) (plus i j) j))))))
      f)))

; int -> t -> t -> [t]
;
; (== (series 5 10 2) (list 10 12 14 16 18))
(define series (series-with +))

; [a] -> [a]
;
; (replicateM 3 (lambda () (shuffle '(1 2 3 4 5))))
(define shuffle
  (lambda (l)
    (let ((q (map (lambda (e) (cons (random-float-uniform 0 1) e)) l))
          (c (lambda (a b) (compare (car a) (car b)))))
      (map cdr (sortBy c q)))))

; [float] -> float -> int
(define w-index
  (lambda (w n)
    (findIndex
     (lambda (e)
       (< n e))
     (integrate w))))

; [a] -> [float] -> a
;
; (replicateM 20 (lambda () (w-choose '(1 2 3 4 5) '(0.4 0.2 0.2 0.1 0.1))))
(define w-choose
  (lambda (l w)
    (list-ref l (w-index w (random-float-uniform 0 1)))))

; [float] -> [float]
(define normalize-sum
  (lambda (l)
    (let ((n (foldl1 + l)))
      (map
       (lambda (e)
	 (/ e n))
       l))))

; [a] -> [float] -> a
(define p-choose
  (lambda (l p)
    (w-choose l (normalize-sum p))))

; [[a]] -> [[a]]
;
; (== (extend-all '((1 2) (1 2 3) (1 2 3 4) (1 2 3 4 5))) '((1 2 1 2 1) (1 2 3 1 2) (1 2 3 4 1) (1 2 3 4 5)))
(define extend-all
  (lambda (l)
    (let* ((n (maximum (map length l))))
      (map (lambda (e) (extend e n)) l))))

; [a|[a]] -> [[a]]
;
; (== (extend-or-replicate-all '(1 (2 3))) '((1 1) (2 3)))
(define extend-or-replicate-all
  (lambda (l)
    (let* ((f (lambda (x) (if (list? x) (length x) 1)))
	   (n (maximum (map f l))))
      (map (lambda (e) (if (list? e) (extend e n) (replicate n e))) l))))

; [a|[a]] -> [[a]]
;
; (== (flop '(1 (2 3))) '((1 2) (1 3)))
; (== (flop '(1 (2 (3 4)))) '((1 2) (1 (3 4))))
(define flop
  (lambda (l)
    (transpose (extend-or-replicate-all l))))

; [t] -> int -> t
;
; (and (== (wrap-at '(1 2 3) -5) 2) (== (mod -5 3) 1))
; (== (map (lambda (index) (wrap-at '(1 2 3) (- index 5))) (iota 9)) '(2 3 1 2 3 1 2 3 1))
(define wrap-at
  (lambda (l n)
    (list-ref l (mod n (length l)))))

(define windex w-index)
(define wchoose w-choose)
(define normalizeSum normalize-sum)
(define pchoose p-choose)
(define wrapAt wrap-at)

(define arrayAt vector-ref)
(define arraySize vector-length)
(define arrayChoose vector-choose)
