; port -> [osc]
(define recv-packet-sequence
  (lambda (fd)
    (let loop ((r (list)))
      (let ((p (recvPacket fd)))
	(if p (loop (cons p r)) (reverse r))))))
