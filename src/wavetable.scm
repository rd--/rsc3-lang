; int -> int -> [a] -> [[a]]
(define segment
  (lambda (n k l)
    (let ((s (take n l)))
      (if (null? s)
	  s
	  (cons s (segment n k (drop k l)))))))

; [float] -> [float]
(define wavetable->signal
  (lambda (l)
    (concatMap sum (segment 2 2 l))))

; [float] -> [float]
(define signal->wavetable
  (lambda (l)
    (let ((f (lambda (e0 e1) (list (- (* 2.0 e0) e1) (- e1 e0)))))
      (concatMap f (segment 1 2 (append l (list (head l))))))))
