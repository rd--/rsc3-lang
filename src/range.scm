; ugen -> bool
(define unipolar?
  (lambda (u)
    (if (mce? u)
	(all unipolar? (mceChannels u))
	(member
	 (ugen-name u)
	 (list
	  "Dust"
	  "Impulse"
	  "LFPulse"
	  "TPulse"
	  "Trig1")))))

; ugen -> ugen -> ugen -> ugen
(define range
  (lambda (u l r)
    (if (unipolar? u)
	(LinLin u 0 1 l r)
	(LinLin u -1 1 l r))))

; ugen -> ugen -> ugen -> ugen
(define expRange
  (lambda (u l r)
    (if (unipolar? u)
	(LinExp u 0 1 l r)
	(LinExp u -1 1 l r))))
