(define random-uniform random-float-uniform)

; () -> float
(define random-linear
  (lambda ()
    (min (random-uniform 0 1)
	 (random-uniform 0 1))))

; () -> float
(define random-inverse-linear
  (lambda ()
    (max (random-uniform 0 1)
         (random-uniform 0 1))))

; () -> float
(define random-triangular
  (lambda ()
    (* 0.5
       (+ (random-uniform 0 1)
          (random-uniform 0 1)))))

; float -> float
(define random-exponential
  (lambda (l)
    (let ((u (random-uniform 0 1)))
      (if (zero? u)
          (random-exponential l)
          (/ (- (log u)) l)))))

; float -> float
(define random-bilinear-exponential
  (lambda (l)
    (let ((u (* 2 (random-uniform 0 1))))
      (if (zero? u)
          (random-bilinear-exponential l)
          (* (if (> u 1) -1 1)
             (log (if (> u 1)
                      (- 2 u)
                      u)))))))

; float -> float -> float
;
; (replicate-m 3 (lambda () (random-gaussian 1 0)))
(define random-gaussian
  (lambda (sigma mu)
    (+ mu
       (* (sqrt (* -2 (log (random-uniform 0 1))))
          (sin (* 2 pi (random-uniform 0 1)))
          sigma))))

; float -> float
(define random-cauchy
  (lambda (alpha)
    (let ((u (random-uniform 0 1)))
      (if (= u 0.5)
          (random-cauchy alpha)
          (* alpha (tan (* u pi)))))))

; float -> float -> float
(define random-beta
  (lambda (a b)
    (let ((u1 (random-uniform 0 1)))
      (if (zero? u1)
          (random-beta a b)
          (let ((u2 (random-uniform 0 1)))
            (if (zero? u2)
                (random-beta a b)
                (let* ((y1 (expt u1 (/ 1 a)))
                       (y2 (expt u2 (/ 1 b)))
                       (sum (+ y1 y2)))
                  (if (> sum 1)
                      (random-beta a b)
                      (/ y1 sum)))))))))

; float -> float -> float
(define random-weibull
  (lambda (s t)
    (let ((u (random-uniform 0 1)))
      (if (or (zero? u)
              (= u 1))
          (random-weibull s t)
          (let ((a (/ 1 (- 1 u))))
            (* s (expt (log a) (/ 1 t))))))))

; float -> float
(define random-poisson
  (lambda (l)
    (let loop ((v (exp (- l)))
               (u (random-uniform 0 1))
               (n 0))
      (if (>= u v)
          (loop v (* u (random-uniform 0 1)) (+ n 1))
          n))))
