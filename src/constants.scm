(define pi2
  (/ pi 2))

(define pi32
  (* pi 1.5))

(define twopi
  (* pi 2))

(define rtwopi
  (/ 1 twopi))

(define log001
  (log 0.001))

(define log01
  (log 0.01))

(define log1
  (log 0.1))

(define rlog2
  (/ 1.0 (log 2.0)))

(define sqrt2
  (sqrt 2.0))

(define rsqrt2
  (/ 1.0 sqrt2))
