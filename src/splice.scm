; tree a -> tree a
;
; (== (splice '(1 (2 3) 4 (5 (6 7)))) '(1 2 3 4 5 (6 7)))
;
; c.f.
;
; (== (flatten '(1 (2 3) 4 (5 (6 7)))) '(1 2 3 4 5 6 7))
(define splice
  (lambda (l)
    (let ((f (lambda (a b)
	       (if (list? a)
		   (append a b)
		   (cons a b)))))
      (foldr f nil l))))
