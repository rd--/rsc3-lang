rsc3-lang
---------

[r6rs](http://haskell.org/)
scheme variants of
[sclang](http://audiosynth.com/)
functions

tested with:
[chezscheme](https://www.scheme.com/)-9.5.4,
[ikarus](http://ikarus-scheme.org/)-0.0.4-1870,
[guile](https://www.gnu.org/software/guile/)-3.0.4

© [rohan drape](http://rohandrape.net/),
  1998-2022,
  [gpl](http://gnu.org/copyleft/)
