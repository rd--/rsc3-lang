; r6rs
(import (rhs core) (rsc3 core) (rsc3 server) (rsc3 ugen) (rsc3 lang))

; overlap-texture.scm
(let ((u (Pan2 (SinOsc (Rand 400 600) 0) (Rand -1 1) 0.02)))
  (withSc3 (overlapTexture (list 6 3 9 24) u)))

(overlap
 (lambda ()
   (Pan2 (SinOsc (Rand 400 600) 0) (Rand -1 1) 0.02)) 6 3 9)

; sndfile.scm
(delete-file "/tmp/sin.au")
(write-snd-file au-f32 48000 1 "/tmp/sin.au" (map sin (enumFromThenTo 0 (/ pi 256) (* pi 2))))
