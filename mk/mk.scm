(import (rnrs) (mk-r6rs core))

(define rsc3-lang-file-seq
  (list
   "collection.scm"
   "constants.scm"
   "overlap-texture.scm"
   "random.scm"
   "range.scm"
   "recv.scm"
   "score.scm"
   "sndfile.scm"
   "spec.scm"
   "splice.scm"
   "warp.scm"
   "wavetable.scm"))

(define rsc3-lang-src
  (map (lambda (x) (string-append "../src/" x)) rsc3-lang-file-seq))

(mk-r6rs '(rsc3 lang)
         rsc3-lang-src
	 (string-append (list-ref (command-line) 1) "/rsc3/lang.sls")
	 '((rnrs) (rhs core) (sosc core) (rsc3 core) (rsc3 server) (rsc3 ugen))
	 '()
	 '())

(exit)
